# 20181129 - Deref.rs \#0

Thu, 29. Nov. 2018

https://derefrs.connpass.com/event/109220/

```toml
[meetup]
name = "Deref Rust"
date = "20181129"
version = "0.0.0"
attendees = [
  "Yasuhiro Asaka <yasuhiro.asaka@grauwoelfchen.net>"
]
repository = "https://gitlab.com/derefrs/reading-logs/20181129-0"
keywords = ["Rust"]

[locations]
park6 = {site = "Roppongi"}
zulip = {site = "https://derefrs.zulipchat.com", optional = true }
```

## Notes

| Name | Snippet / Note / What I did in few words |
|--|--|
| @grauwoelfchen | https://gitlab.com/derefrs/reading-logs/20181129-0/snippets/1785431 |


## Links

* [Deref Rust - GitLab.com](https://gitlab.com/derefrs)
* [Deref Rust - Zulip Chat](https://derefrs.zulipchat.com/)

## License

`MIT`

```text
Reading Logs
Copyright (c) 2018 Deref.rs

This is free software: You can redistribute it and/or modify
it under the terms of the MIT License.
```
